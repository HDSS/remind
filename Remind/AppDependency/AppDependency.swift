//
//  AppDependency.swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

struct AppDependencies: HasTimerNotification, HasDateNotification, HasLocationNotification, HasCLAuthenticationService, HasCLUpdateLocationService,
                                                                                                                        HasUNAuthenticationService {
    
    let timerNotification: TimerNotification
    let dateNotification: DateNotification
    let locationNotification: LocationNotification
    let clAuthenticationService: CLAuthenticationService
    let clUpdateLocationService: CLUpdateLocationService
    let unAuthenticationService: UNAuthenticationService
    
    
    init(timerNotify: TimerNotification,
         dateNotify: DateNotification,
         locationNotify: LocationNotification,
         clAuth: CLAuthenticationService,
         clUpdate: CLUpdateLocationService,
         unAuth: UNAuthenticationService) {
        
        timerNotification = timerNotify
        dateNotification = dateNotify
        locationNotification = locationNotify
        clAuthenticationService = clAuth
        clUpdateLocationService = clUpdate
        unAuthenticationService = unAuth
        
    }
    
}
