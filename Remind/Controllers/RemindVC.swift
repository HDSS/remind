//
//  ViewController.swift
//  Remind
//
//  Created by Michał Piotrowski on 28.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import UIKit

class RemindVC: UIViewController {
    
    var remindViewModel: RemindViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(RemindVC.handleNotification(_:)),
                                               name: NSNotification.Name(NotificationConstant.notificationName),
                                               object: nil)
        
        remindViewModel.delegate = self
        remindViewModel.authorizeLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    //MARK: Events
    
    @IBAction func timerPressed(_ sender: UIButton) {
        showActionSheet(title: "5 seconds") {
            self.remindViewModel.sendTimerNotification(timeInterval: 5)
        }
    }
    
    @IBAction func datePressed(_ sender: UIButton) {
        showActionSheet(title: "Some future date") {
            var components = DateComponents()
            components.second = 0
            
            self.remindViewModel.sendDateNotification(dateComponents: components)
        }
    }
    
    @IBAction func locationPressed(_ sender: UIButton) {
        showActionSheet(title: "When I return") {
            self.remindViewModel.updateLocation()
        }
    }
    
    //MARK: Notification
    @objc
    func handleNotification(_ sender: Notification) {
        guard let action = sender.object as? NotificationActionEnum else { return }
        
        switch action {
            
        case .timer: view.changeBackgroundColor(color: UIColor.blue)
        case .date: view.changeBackgroundColor(color: UIColor.black)
        case .location: view.changeBackgroundColor(color: UIColor.red)
            
        }
        
    }

}

extension RemindVC: RemindViewModelDelegate {
    
    func didEnterCLRegion() {
        self.remindViewModel.sendLocationNotification()
    }
}

