//
//  AppCoordinator.swift
//  Remind
//
//  Created by Michał Piotrowski on 28.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import UIKit

final class AppCoordinator: Coordinator {
    
//    let appDependencies: AppDependencies
    
    func start() {
        
        let coordinator = RemindCoordinator(navigationController: navigationController, appDependencies: appDependencies)
        childCoordinators.append(coordinator)
        coordinator.start()
        
    }
}
