//
//  Coordinator.swift
//  Remind
//
//  Created by Michał Piotrowski on 28.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import UIKit

class Coordinator {
    
    var childCoordinators: [Coordinator] = []
    weak var navigationController: UINavigationController?
    
    var appDependencies: AppDependencies
    
    init(navigationController: UINavigationController?, appDependencies: AppDependencies) {
        
        self.navigationController = navigationController
        self.appDependencies = appDependencies
    }
}

