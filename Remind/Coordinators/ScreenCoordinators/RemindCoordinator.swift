//
//  RemindCoordinator.swift
//  Remind
//
//  Created by Michał Piotrowski on 28.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import UIKit

final class RemindCoordinator: Coordinator {
    
    func start() {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let remindVC = storyboard.instantiateInitialViewController() as? RemindVC else { return }
        
        let viewModel = RemindViewModel(dependencies: appDependencies)
        remindVC.remindViewModel = viewModel
        
        navigationController?.pushViewController(remindVC, animated: true)
    }
}
