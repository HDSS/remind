//
//  UIView+Background.swift
//  Remind
//
//  Created by Michał Piotrowski on 11.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import UIKit

extension UIView {
    
    func changeBackgroundColor(color: UIColor) {
        self.backgroundColor = color
        
    }
}
