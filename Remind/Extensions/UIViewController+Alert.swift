//
//  UIViewController+Alert.swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showActionSheet(title: String, completion: @escaping () -> ()) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: title, style: .default) { (_) in
            completion()
        }
        alert.addAction(action)
        
        self.present(alert, animated: true)
    }
}
