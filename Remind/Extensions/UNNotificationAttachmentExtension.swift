//
//  UNNotificationAttachmentExtension.swift
//  Remind
//
//  Created by Michał Piotrowski on 07.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation
import UserNotifications

extension UNNotificationAttachment {
    
    convenience init?(for id: AttachmentEnum) {
        
        guard let url = Bundle.main.url(forResource: id.imageName(), withExtension: "png") else { return nil }
        
        try? self.init(identifier: id.imageName(), url: url)
    }
}
