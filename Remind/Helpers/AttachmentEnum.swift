//
//  AttachmentEnum.swift
//  Remind
//
//  Created by Michał Piotrowski on 06.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

enum AttachmentEnum: String {
    
    case timer = "userNotification.attachment.timer"
    case date = "userNotification.attachment.date"
    case location = "userNotification.attachment.location"
}

extension AttachmentEnum {
    
    func imageName() -> String {
        
        switch self {
            
        case .timer: return "TimeAlert"
        case .date: return "DateAlert"
        case .location: return "LocationAlert"
            
        }
    }
    
}
