//
//  NotificationActionEnum.swift
//  Remind
//
//  Created by Michał Piotrowski on 11.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

enum NotificationActionEnum: String {
    
    case timer = "userNotification.action.timer"
    case date = "userNotification.action.date"
    case location = "userNotification.action.location"
}

