//
//  NotificationCategoryEnum.swift
//  Remind
//
//  Created by Michał Piotrowski on 11.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

enum NotificationCategoryEnum: String {
    
    case timer = "userNotification.category.timer"
    case date = "userNotification.category.date"
    case location = "userNotification.category.location"
}
