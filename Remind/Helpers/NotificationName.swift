//
//  NotificationNameEnum.swift
//  Remind
//
//  Created by Michał Piotrowski on 11.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

public struct NotificationConstant {
    
    static let notificationName = "internalNotification.handleAction"
}

