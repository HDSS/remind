//
//  CLUpdateLocationService.swift
//  Remind
//
//  Created by Michał Piotrowski on 04.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

protocol HasCLUpdateLocationService {
    
    var clUpdateLocationService: CLUpdateLocationService { get }
    
}
