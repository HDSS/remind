//
//  LocationDependency.swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

protocol HasLocationNotification {
    
    var locationNotification: LocationNotification { get }
    
}
