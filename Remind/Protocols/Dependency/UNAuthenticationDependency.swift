//
//  UNAuthenticationDependency.swift
//  Remind
//
//  Created by Michał Piotrowski on 06.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

protocol HasUNAuthenticationService {
    
    var unAuthenticationService: UNAuthenticationService { get }
    
}
