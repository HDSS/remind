//
//  Notifying .swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation

protocol Notifying  {
    
    associatedtype Value
    
    func sendNotification(value: Value)
}
