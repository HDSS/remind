//
//  CLAuthentication.swift
//  Remind
//
//  Created by Michał Piotrowski on 04.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation
import CoreLocation

final class CLAuthenticationService {
    
    let coreLocationManager = CLLocationManager()
    
    public func authorize() {
        coreLocationManager.requestAlwaysAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            coreLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        }
    }
}

