//
//  CLUpdateLocationService.swift
//  Remind
//
//  Created by Michał Piotrowski on 04.12.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation
import CoreLocation

private struct Constants {
    static let regionID = "regionID"
    static let radius = 20.0
}

protocol CLUpdateLocationDelegate {
    func didEnterCLRegion()
}

class CLUpdateLocationService: NSObject {
    
    let coreLocationManager = CLLocationManager()
    private var shouldSetRegion = true
    
    var delegate: CLUpdateLocationDelegate?
    
    func updateLocation() {
        shouldSetRegion = true
        coreLocationManager.delegate = self
        coreLocationManager.startUpdatingLocation()
    }
}

extension CLUpdateLocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let currentLocation = locations.first, shouldSetRegion else { return }
        shouldSetRegion = false
        
        let region = CLCircularRegion(center: currentLocation.coordinate, radius: Constants.radius, identifier: Constants.regionID)
        manager.startMonitoring(for: region)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        delegate?.didEnterCLRegion()
    }
}
