//
//  UNAuthorization.swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation
import UserNotifications

final class UNAuthenticationService: NSObject  {
    
    private var userNotificationCenter = UNUserNotificationCenter.current()

    //MARK: Authorization & Configuration
    public func authorize() {
        
        let options: UNAuthorizationOptions = [.alert, .sound, .carPlay, .badge]
        
        userNotificationCenter.requestAuthorization(options: options) { (success, error) in
            guard success else { return }
            
            self.configure()
        }
    }
    
    public func configure() {
        userNotificationCenter.delegate = self
    }

}

extension UNAuthenticationService: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let action = NotificationActionEnum(rawValue: response.actionIdentifier) {
            NotificationCenter.default.post(name: NSNotification.Name(NotificationConstant.notificationName),
                                            object: action)
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let options: UNNotificationPresentationOptions = [.alert, .sound]
        completionHandler(options)
    }
}
