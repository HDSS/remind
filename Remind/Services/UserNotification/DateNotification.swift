//
//  DateNotification.swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation
import UserNotifications

private struct Constants {
    //* Date trigger
    static let dateContentTitle = "Date Trigger"
    static let dateContentBody = "It is now the future"
    static let dateRequestIdentifier = "userNotification.date"
    
    //* Date action
    static let dateActionTitle = "Run Date Logic"
}

struct DateNotification: Notifying {
    
    typealias Value = DateComponents
    
    func sendNotification(value: DateComponents) {
        
        let content = UNMutableNotificationContent()
        content.title = Constants.dateContentTitle
        content.body = Constants.dateContentBody
        content.badge = 1 //just for test
        content.sound = .default()
        
        if let attachment = UNNotificationAttachment(for: .date) {
            content.attachments = [attachment]
        }
        
        let dateAction = UNNotificationAction(identifier: NotificationActionEnum.date.rawValue,
                                               title: Constants.dateActionTitle,
                                               options: [.destructive])
        
        let dateCategory = UNNotificationCategory(identifier: NotificationActionEnum.date.rawValue, actions: [dateAction], intentIdentifiers: [])
        content.categoryIdentifier = dateCategory.identifier
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: value,
                                                    repeats: false)
        
        let request = UNNotificationRequest(identifier: Constants.dateRequestIdentifier,
                                            content: content,
                                            trigger: trigger)
        
        UNUserNotificationCenter.current().setNotificationCategories([dateCategory])
        UNUserNotificationCenter.current().add(request)
    }
}

