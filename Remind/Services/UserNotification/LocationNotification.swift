//
//  LocationNotification.swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation
import CoreLocation
import UserNotifications

private struct Constants {
    //* Location trigger
    static let locationContentTitle = "Location Trigger"
    static let locationContentBody = "Welcome Back"
    static let locationRequestIdentifier = "userNotification.location"
    
    //* Location action
    static let locationActionTitle = "Run  Location Logic"
}

struct LocationNotification {
    
    private var shouldSetRegion = false
    
    func sendNotification() {
        
        let content = UNMutableNotificationContent()
        content.title = Constants.locationContentTitle
        content.body = Constants.locationContentBody
        content.badge = 1 //just for test
        content.sound = .default()
        
        if let attachment = UNNotificationAttachment(for: .location) {
            content.attachments = [attachment]
        }
        
        let locationAction = UNNotificationAction(identifier: NotificationActionEnum.date.rawValue,
                                              title: Constants.locationActionTitle,
                                              options: [.foreground])
        
        let locationCategory = UNNotificationCategory(identifier: NotificationActionEnum.location.rawValue, actions: [locationAction], intentIdentifiers: [])
        content.categoryIdentifier = locationCategory.identifier
        
        let request = UNNotificationRequest(identifier: Constants.locationRequestIdentifier,
                                            content: content,
                                            trigger: nil)
        
        UNUserNotificationCenter.current().setNotificationCategories([locationCategory])
        UNUserNotificationCenter.current().add(request)
    }
}

