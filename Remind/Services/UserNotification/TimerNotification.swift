//
//  TimerNotification.swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation
import UserNotifications

private struct Constants {    
    //* Timer trigger
    static let timerContentTitle = "Timer Trigger"
    static let timerContentBody = "Your timer is done"
    static let timerRequestIdentifier = "userNotification.timer"
    
    //* Timer action
    static let timerActionTitle = "Run Timer Logic"
}

struct TimerNotification: Notifying {
    
    typealias Value = TimeInterval
    
    func sendNotification(value: TimeInterval) {
        
        let content = UNMutableNotificationContent()
        content.title = Constants.timerContentTitle
        content.body = Constants.timerContentBody
        content.badge = 1 //just for test
        content.sound = .default()
        
        if let attachment = UNNotificationAttachment(for: .timer) {
            content.attachments = [attachment]
        }

        let timerAction = UNNotificationAction(identifier: NotificationActionEnum.timer.rawValue,
                                          title: Constants.timerActionTitle,
                                          options: [.authenticationRequired])
        
        let timerCategory = UNNotificationCategory(identifier: NotificationActionEnum.timer.rawValue, actions: [timerAction], intentIdentifiers: [])
        content.categoryIdentifier = timerCategory.identifier
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: value,
                                                        repeats: false)
        
        let request = UNNotificationRequest(identifier: Constants.timerRequestIdentifier,
                                            content: content,
                                            trigger: trigger)
        
        UNUserNotificationCenter.current().setNotificationCategories([timerCategory])
        UNUserNotificationCenter.current().add(request)
    }
}
