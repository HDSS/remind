//
//  RemindViewModel.swift
//  Remind
//
//  Created by Michał Piotrowski on 30.11.2017.
//  Copyright © 2017 Michał Piotrowski. All rights reserved.
//

import Foundation
import CoreLocation

protocol RemindViewModelDelegate: class {
    func didEnterCLRegion()
}

class RemindViewModel {
    
    typealias Dependencies = HasTimerNotification & HasDateNotification & HasLocationNotification & HasCLAuthenticationService & HasCLUpdateLocationService
    
    weak var delegate: RemindViewModelDelegate?
    
    var remindVMDependencies: Dependencies
    
    init(dependencies: Dependencies) {
        
        remindVMDependencies = dependencies
        remindVMDependencies.clUpdateLocationService.delegate = self
    }
    
    //MARK: Notifications
    func sendTimerNotification(timeInterval: TimeInterval) {
        
        remindVMDependencies.timerNotification.sendNotification(value: timeInterval)
    }
    
    func sendDateNotification(dateComponents: DateComponents) {
        
        remindVMDependencies.dateNotification.sendNotification(value: dateComponents)
    }
    
    func sendLocationNotification() {
        
        remindVMDependencies.locationNotification.sendNotification()
    }
    
    func authorizeLocation() {
        
        remindVMDependencies.clAuthenticationService.authorize()
    }
    
    func updateLocation() {
        
        remindVMDependencies.clUpdateLocationService.updateLocation()
    }
}

extension RemindViewModel: CLUpdateLocationDelegate {
    
    func didEnterCLRegion() {
        
        delegate?.didEnterCLRegion()
    }
}

